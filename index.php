<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Gallery</title>
	</head>
	<body>
<?php
require_once dirname(__FILE__).'/lib/Gallery.php';
$gallery = new Gallery(array (
	'base_uri' => './assets/gfx/gallery'
));
$gallery->gallery();
?>
	</body>
</html>