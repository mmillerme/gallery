<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Gallery</title>
	</head>
	<body>
<?php
require_once dirname(__FILE__).'/lib/Gallery.php';
	// Load the Gallery and Gallery_Image classes

/**
 * KNOWN FEATURES/LIMITATIONS
 *  - Do not use the same images for galleries with different thumbnail/full-size dimensions.
 *  - You may load multiple galleries on the same page.
 *  - Use only ASCII characters in filenames.
 *  - Do not use the same filename with different extensions, e.g. 'photo.jpg' and 'photo.gif'.
 *    The script will treat them as the same file.
 */

$gallery = new Gallery(array (
	'base_uri' => './assets/gfx/gallery',
		/* The base URI for href and src attributes. Trailing slash optional.
		   Can be in any of these formats:
			- '', 'path/to/dir', or '../path/to/dir' --> Generates paths
			   relative to the location of Gallery.php.
			- '.', './path/to/dir', or './../path/to/dir' --> Generates
			   paths relative to the URL of the page.
			- '/' or '/path/to/dir' --> Generates paths relative to the
			   the domain name root. */
	'queue_directory' => 'queue',
		/* The path (relative to base_uri) to the directory that will contain
		   images to be processed (they will be deleted). */
	'thumbnail_directory' => 'thumbnails',
		/* The path (relative to base_uri) to the directory that will contain
		   generated thumbnails. */
	'thumbnail_width' => 200,
		/* The maximum width (in pixels) of a generated thumbnail. */
	'thumbnail_height' => 200,
		/* The maximum height (in pixels) of a generated thumbnail. */
	'thumbnail_resize' => Gallery::RESIZE_CROP,
		/* One of three values that determines what engine to use to resize the
		   thumbnail:
			- Gallery::RESIZE_CROP to resize the image to the target dimensions,
				centering and cropping excess.
			- Gallery::RESIZE_ASPECT to resize the image until it is within
				the maximum dimension boundaries, maintaining proportions.
			- Gallery::RESIZE_NONE to not resize at all. */
	'thumbnail_quality' => 70,
		/* The quality to reduce to during resize (0-100). */

	'full_directory' => 'full',
		/* The path (relative to base_uri) to the directory that will contain
		   full-size images. */
	'full_width' => 1024,
		/* The maximum width (in pixels) of a full-size image. */
	'full_height' => 768,
		/* The maximum height (in pixels) of a full-size image. */
	'full_resize' => Gallery::RESIZE_ASPECT,
		/* One of three values that determines what engine to use to resize the
		   full-size image:
			- Gallery::RESIZE_CROP to resize the image to the target dimensions,
				centering and cropping excess.
			- Gallery::RESIZE_ASPECT to resize the image until it is within
				the maximum dimension boundaries, maintaining proportions).
			- Gallery::RESIZE_NONE to not resize at all. */
	'full_quality' => 90,
		/* The quality to reduce to during resize (0-100). */

	'class' => 'lightbox',
		/* The class applied to the <a> elements. */
	'title' => true,
		/* If true, adds the filename (minus the extension) to the title attribute of
		   the <a> element. */
	'xhtml' => false,
		/* If true, generates XHTML instead of HTML. */

	'pagination_per' => Gallery::PAGINATION_NONE,
		/* The number of thumbnails to place on each page. Set to
		   Gallery::PAGINATION_NONE to disable pagination. */
	'pagination_query' => 'gallery_%_page',
		/* The parameter applied to the query string to represent the current
		   page number. % gets replaced with the gallery number. */

	'template_gallery' => function ($instance, $_start, $_stop, $_pageNum) {
		echo '<div class="gallery gallery-'.$instance->num.' gallery-page-'.$_pageNum.'">'.PHP_EOL;
		for ($i = $_start, $j = 1; $i < $_stop; ++$i, ++$j) {
			$image = $instance->images[$i];
			$uri = htmlentities($image->fullURI);
			$class = 'gallery-photo gallery-photo-'.($i+1).' gallery-spot-'.$j.($instance->options['class'] ? ' '.htmlentities($instance->options['class']) : '');
			$title = $instance->options['title'] ? ' title="'.htmlentities($image->title).'"' : '';
			$src = htmlentities($image->thumbnailURI);
			$alt = htmlentities($image->title);
			$width = $image->thumbnailWidth;
			$height = $image->thumbnailHeight;
			$close = $instance->options['xhtml'] ? ' /' : '';

			echo chr(9).'<a href="'.$uri.'" class="'.$class.'"'.$title.'><img src="'.$src.'" alt="'.$alt.'" width="'.$width.'" height="'.$height.'"'.$close.'></a>'.PHP_EOL;
		}
		echo '</div>'.PHP_EOL;
	},
		/* A function called to output gallery markup. Passes Gallery instance, start
		   index, end index, and page number as the parameters. */

	'template_pagination' => function ($instance, $_start, $_stop) {
		$param = str_replace('%', $instance->num, $instance->options['pagination_query']);
		echo '<ul>'.PHP_EOL;
		for ($i = $_start+1; $i < $_stop+1; ++$i) {
			$query = htmlentities(http_build_query(array ($param => $i)+$_GET));
			echo chr(9).'<li><a href="?'.$query.'">'.$i.'</a></li>'.PHP_EOL;
		}
		echo '</ul>'.PHP_EOL;
	}
		/* A function called to output pagination markup. Passes Gallery instance, start
		   index, and end index as the parameters. */
));

$gallery->gallery();
	/* Output the gallery HTML. */

$gallery->pagination();
	/* Output the pagination HTML. */
?>
	</body>
</html>