<?php
/**
 * Gallery.php, the Gallery class and Gallery_Image class
 *
 * Contains 2 utility classes to automatically generate image galleries. Resizes thumbnails and full-
 * size images. Generates the markup for a thumbnail gallery. Interprets relative paths intelligently.
 * Can maintain aspect ratio or crop to target dimensions. Supports pagination. Supports GIFs, JPEGS,
 * and PNGs. Generated image quality controllable for optimization. Supports lightbox.
 *
 * KNOWN FEATURES/LIMITATIONS
 *  - Do not use the same images for galleries with different thumbnail/full-size dimensions.
 *  - You may load multiple galleries on the same page.
 *  - Use only ASCII characters in filenames.
 *  - Do not use the same filename with different extensions, e.g. 'photo.jpg' and 'photo.gif'.
 *    The script will treat them as the same file.
 *
 * @author M Miller <me@mmiller.me>
 * @version 1.2
 */

/**
 * A utility class to automatically generate image galleries. Resizes thumbnails and full-size
 * images. Generates the markup for a thumbnail gallery. Interprets relative paths intelligently.
 * Can maintain aspect ratio or crop to target dimensions. Supports pagination. Supports GIFs, JPEGs,
 * and PNGs. Generated image quality controllable for optimization. Supports lightbox.
 */
class Gallery {
	private
		/*
		 * The absolute path to the directory that holds full-size images and thumbnails.
		 * (These may be in subdirectories or may be relative to outside this directory.)
		 * This is calculated based on the value of the base_uri option.
		 * @var string
		 */
		$base,
		/**
		 * An associative array of configurable options. All options must match the key
		 * and type value of the defaults. Some constants are provided to assist. The
		 * options are as follows:
		 * @var string 'base_uri'
		 *      The base URI for href and src attributes. Trailing slash optional.
		 *      Can be in any of these formats:
		 *       - '', 'path/to/dir', or '../path/to/dir' --> Generates paths
		 *         relative to the location of Gallery.php.
		 *       - '.', './path/to/dir', or './../path/to/dir' --> Generates
		 *         paths relative to the URL of the page.
		 *       - '/' or '/path/to/dir' --> Generates paths relative to the
		 *         the domain name root.
		 *       Default: ''
		 * @var string 'queue_directory'
		 *      The path (relative to base_uri) to the directory that will contain
		 *      images to be processed (they will be deleted).
		 *      Default: 'queue'
		 * @var string 'full_directory'
		 *      The path (relative to base_uri) to the directory that will contain
		 *      full-size images.
		 *      Default: 'full'
		 * @var string 'thumbnail_directory'
		 *      The path (relative to base_uri) to the directory that will contain
		 *      generated thumbnails.
		 *      Default: 'thumbnails'
		 * @var int 'thumbnail_width'
		 *      The maximum width (in pixels) of a generated thumbnail.
		 *      Default: 200
		 * @var int 'thumbnail_height'
		 *      The maximum height (in pixels) of a generated thumbnail.
		 *      Default: 200
		 * @var string 'thumbnail_resize'
		 *      One of three values that determines what engine to use to resize the
		 *      thumbnail:
		 *       Gallery::RESIZE_CROP to resize the image to the target dimensions,
		 *         centering and cropping excess.
		 *       Gallery::RESIZE_ASPECT to resize the image until it is within
		 *         the maximum dimension boundaries, maintaining proportions).
		 *       Gallery::RESIZE_NONE to not resize at all.
		 *      Default: Gallery::RESIZE_CROP
		 * @var int 'thumbnail_quality'
		 *      The quality to reduce to during resize (0-100).
		 *      Default: 70
		 * @var string 'full_directory'
		 *      The path (relative to base_uri) to the directory that will contain
		 *      the full-size images.
		 *      Default: 'full'
		 * @var int 'full_width'
		 *      The maximum width (in pixels) of a full-size image.
		 *      Default: 1024
		 * @var int 'full_height'
		 *      The maximum height (in pixels) of a full-size image.
		 *      Default: 768
		 * @var string 'full_resize'
		 *      One of three values that determines what engine to use to resize the
		 *      full-size image:
		 *       Gallery::RESIZE_CROP to resize the image to the target dimensions,
		 *         centering and cropping excess.
		 *       Gallery::RESIZE_ASPECT to resize the image until it is within
		 *         the maximum dimension boundaries, maintaining proportions).
		 *       Gallery::RESIZE_NONE to not resize at all.
		 *      Default: Gallery::RESIZE_ASPECT
		 * @var int 'full_quality'
		 *      The quality to reduce to during resize (0-100).
		 *      Default: 90
		 * @var string 'class'
		 *      The class applied to the <a> elements.
		 *      Default: lightbox
		 * @var bool 'title'
		 *      If true, adds the filename (minus the extension) to the title attribute of
		 *      the <a> element.
		 *      Default: true
		 * @var bool 'xhtml'
		 *      If true, generates XHTML instead of HTML.
		 *      Default: false
		 * @var int 'pagination_per'
		 *      The number of thumbnails to place on each page. Set to
		 *      Gallery::PAGINATION_NONE to disable pagination.
		 *      Default: Gallery::PAGINATION_NONE
		 * @var string 'pagination_query'
		 *      The parameter applied to the query string to represent the current
		 *      page number. % gets replaced with the gallery number.
		 *      Default: 'gallery_%_page'
		 * @var function 'template_gallery'
		 *      A function called to output gallery markup. Passes Gallery instance, start
		 *      index, end index, and page number as the parameters. See __construct for
		 *      default.
		 * @var function 'pagination_gallery'
		 *     A function called to output pagination markup. Passes Gallery instance, start
		 *     index, and end index as the parameters. See __construct for default.
		 */
		$options,
		/**
		 * An array of Gallery_Image objects that compose the gallery.
		 * @var array
		 */
		$images,
		/**
		 * The unique incremental count of the image. See $count for explanation.
		 * @var int
		 */
		$num;

	private static
		/**
		 * A counter that keeps track of the number of gallery instances. This number is
		 * applied to the class to maintain uniqueness and is used in pagination queries.
		 * @var int
		 */
		$count = 0;

	const
		RESIZE_CROP = 'crop',
		RESIZE_ASPECT = 'aspect',
		RESIZE_NONE = '',
		PAGINATION_NONE = 0;

	/**
	 * Creates an instance of Gallery.
	 * @param array $options An associative array of options. See
	 *  aforementioned docs of key/value pairs and their defaults.
	 * @return instance
	 * @throws Exception If any invalid parameters
	 */
	public function __construct ($options = array ()) {
		$this->num = ++self::$count;

		$defaults = array (
			'base_uri' => '',
			'queue_directory' => 'queue',
			'thumbnail_directory' => 'thumbnails',
			'thumbnail_width' => 200,
			'thumbnail_height' => 200,
			'thumbnail_resize' => self::RESIZE_CROP,
			'thumbnail_quality' => 70,
			'full_directory' => 'full',
			'full_width' => 1024,
			'full_height' => 768,
			'full_resize' => self::RESIZE_ASPECT,
			'full_quality' => 90,
			'class' => 'lightbox',
			'title' => true,
			'xhtml' => false,
			'pagination_per' => self::PAGINATION_NONE,
			'pagination_query' => 'gallery_%_page',
			'template_gallery' => function ($instance, $_start, $_stop, $_pageNum) {
				echo '<div class="gallery gallery-'.$instance->num.' gallery-page-'.$_pageNum.'">'.PHP_EOL;
				for ($i = $_start, $j = 1; $i < $_stop; ++$i, ++$j) {
					$image = $instance->images[$i];
					$uri = htmlentities($image->fullURI);
					$class = 'gallery-photo gallery-photo-'.($i+1).' gallery-spot-'.$j.($instance->options['class'] ? ' '.htmlentities($instance->options['class']) : '');
					$title = $instance->options['title'] ? ' title="'.htmlentities($image->title).'"' : '';
					$src = htmlentities($image->thumbnailURI);
					$alt = htmlentities($image->title);
					$width = $image->thumbnailWidth;
					$height = $image->thumbnailHeight;
					$close = $instance->options['xhtml'] ? ' /' : '';

					echo chr(9).'<a href="'.$uri.'" class="'.$class.'"'.$title.'><img src="'.$src.'" alt="'.$alt.'" width="'.$width.'" height="'.$height.'"'.$close.'></a>'.PHP_EOL;
				}
				echo '</div>'.PHP_EOL;
			},
			'template_pagination' => function ($instance, $_start, $_stop) {
				$param = str_replace('%', $instance->num, $instance->options['pagination_query']);
				echo '<ul>'.PHP_EOL;
				for ($i = $_start+1; $i < $_stop+1; ++$i) {
					$query = htmlentities(http_build_query(array ($param => $i)+$_GET));
					echo chr(9).'<li><a href="?'.$query.'">'.$i.'</a></li>'.PHP_EOL;
				}
				echo '</ul>'.PHP_EOL;
			}
		);

		if (!is_array($options)) {
			throw new Exception('Parameter of Gallery::__construct is $options and must be of type array.');
		}

		foreach ($options as $key => $value) {
			if (!isset($defaults[$key])) {
				throw new Exception('$options['.$key.'] of Gallery::__construct does not exist.');
			}
			if (is_callable($defaults[$key]) && !is_callable($value)) {
				throw new Exception('Value of $options['.$key.'] of Gallery::__construct must be of type function.');
			}
			if (gettype($value) !== ($type = gettype($defaults[$key]))) {
				throw new Exception('Value of $options['.$key.'] of Gallery::__construct must be of type '.$type.'.');
			}
		}

		$this->options = $options+$defaults;

		$this->options['base_uri'] = str_replace('\\', '/', $this->options['base_uri']);

		$len = strlen($this->options['base_uri']);
		$first1 = $len > 0 ? substr($this->options['base_uri'], 0, 1) : '';
		$first2 = $len > 1 ? substr($this->options['base_uri'], 0, 2) : '';
		$first3 = $len > 2 ? substr($this->options['base_uri'], 0, 3) : '';
		$ts = $len == 0 || substr($this->options['base_uri'], -1) == '/' ? '' : '/';

		if (($this->options['base_uri'] == '') || ($first1 != '/' && $first1 != '.') || ($first3 == '../')) {
			$gallery = __FILE__;
			$page = $_SERVER['SCRIPT_FILENAME'];

			$list = array ($gallery, $page);
			$arr = array ();
			foreach ($list as $i => $path) {
				$path = str_replace('\\', '/', $path);
				$list[$i] = explode('/', $path);
				unset($list[$i][0]);
				$arr[$i] = count($list[$i]);
			}
			$min = min($arr);
			for ($i = 0; $i < count($list); ++$i) {
				while (count($list[$i]) > $min) {
					array_pop($list[$i]);
				}
				$list[$i] = '/'.implode('/', $list[$i]);
			}
			$list = array_unique($list);
			while (count($list) !== 1) {
				$list = array_map('dirname', $list);
				$list = array_unique($list);
			}
			$commonLen = strlen($list[0])+2;
			$gallery = substr($gallery, $commonLen);
			$page = substr($gallery, $commonLen);

			$uri = $this->options['base_uri'].$ts;
			$this->options['base_uri'] = str_replace('\\', '/', str_repeat('../', substr_count($page, '/')).dirname($gallery).'/'.$uri);
			if (strlen($this->options['base_uri']) && substr($this->options['base_uri'], 0, 1) == '/') {
				$this->options['base_uri'] = substr($this->options['base_uri'], 1);
			}
			$this->base = str_replace('\\', '/', dirname(__FILE__).'/'.$uri);
		}
		else if ($first2 == './') {
			$this->options['base_uri'] = substr($this->options['base_uri'], 2);
			$page = $_SERVER['PHP_SELF'];
			$page = str_replace('\\', '/', $page);
			if (strlen($page) && substr($page, -1) != '/') {
				$page = dirname($page);
			}
			$uri = $this->options['base_uri'].$ts;
			$this->options['base_uri'] = $page.'/'.$uri;
			$page = $_SERVER['SCRIPT_FILENAME'];
			$page = str_replace('\\', '/', $page);
			$this->base = dirname($page).'/'.$uri;
		}
		else {
			$this->options['base_uri'] = $this->options['base_uri'].$ts;
			$root = $_SERVER['DOCUMENT_ROOT'];
			$root = str_replace('\\', '/', $root);
			$this->base = $root.(substr($root, -1) != '/' ? '/' : '').$this->options['base_uri']; 
		}

		foreach (array ('queue', 'full', 'thumbnail') as $path) {
			$this->options[$path.'_directory'] = str_replace('\\', '/', $this->options[$path.'_directory']);
			if (strlen($this->options[$path.'_directory']) > 1 && substr($this->options[$path.'_directory'], -1) != '/') {
				$this->options[$path.'_directory'] .= '/';
			}
		}

		if (!file_exists($p = $this->base.$this->options['queue_directory'])) {
			throw new Exception('Path to queue directory ('.$p.') does not exist.');
		}
		if (!file_exists($p = $this->base.$this->options['full_directory'])) {
			mkdir($p, 0755, true);
		}
		if (!file_exists($p = $this->base.$this->options['thumbnail_directory'])) {
			mkdir($p, 0755, true);
		}

		ini_set('memory_limit', -1);
		set_time_limit(0);
		$this->images = array ();
		$files = array_merge(
			glob($this->base.$this->options['queue_directory'].'*{.gif,*.GIF,*.jpg,*.JPG,*.jpeg,*.JPEG,*.png,*.PNG}', GLOB_BRACE),
			glob($this->base.$this->options['full_directory'].'*{.gif,*.GIF,*.jpg,*.JPG,*.jpeg,*.JPEG,*.png,*.PNG}', GLOB_BRACE)
		);
		usort($files, function ($a, $b) {
			$a = basename($a);
			$b = basename($b);
			return $a < $b ? -1 : 1;
		});
		$names = array ();
		foreach ($files as $path) {
			if (in_array(basename($path), $names)) {
				continue;
			}
			$names[] = basename($path);
			$this->images[] = new Gallery_Image($path, $this);
		}
	}

	/**
	 * A magic method that returns the value of the requested property of this instance.
	 * @var string $property The name of the property.
	 * @return mixed The value of the property
	 * @throws Exception If property does not exist
	 */
	public function __get ($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
		throw new Exception('Property '.$property.' of class Gallery does not exist.');
	}

	/**
	 * Outputs the gallery HTML.
	 * @return void
	 */
	public function gallery () {
		$count = count($this->images);
		$numPages = $this->options['pagination_per'] > 0 ? ceil($count/$this->options['pagination_per']) : 1;
		$param = str_replace('%', $this->num, $this->options['pagination_query']);
		$curPage = isset($_GET[$param]) ? intval($_GET[$param]) : 1;
		$curPage = $curPage > $numPages || $curPage < 1 ? 1 : $curPage;
		$offset = ($curPage-1)*$this->options['pagination_per'];
		$per = $this->options['pagination_per'] > 0 ? $this->options['pagination_per'] : $count;

		$_start = $offset;
		$_stop = min($offset+$per, $count);
		$_pageNum = $curPage;

		$fn = $this->options['template_gallery'];
		$fn($this, $_start, $_stop, $_pageNum);
	}

	/**
	 * Outputs the pagination HTML.
	 * @return void
	 */
	public function pagination () {
		if ($this->options['pagination_per'] == self::PAGINATION_NONE) {
			return;
		}

		$_start = 0;
		$_stop = ceil(count($this->images)/$this->options['pagination_per']);

		$fn = $this->options['template_pagination'];
		$fn($this, $_start, $_stop);
	}
}

/**
 * A class that represents a single image of the Gallery. Used internally.
 */
class Gallery_Image {
	private
		/**
		 * The absolute path to the full-size image.
		 * @var string
		 */
		$path,
		/**
		 * The related Gallery instance.
		 * @var Gallery
		 */
		$instance,
		/**
		 * The relative URI to the full-size image.
		 * @var string
		 */
		$fullURI,
		/**
		 * The title of the image (extracted from filename).
		 * @var string
		 */
		$title,
		/**
		 * The relative URI to the thumbnail.
		 * @var string
		 */
		$thumbnailURI,
		/**
		 * The width (in pixels) of the thumbnail.
		 * @var int
		 */
		$thumbnailWidth,
		/**
		 * The height (in pixels) of the thumbnail.
		 * @var int
		 */
		$thumbnailHeight;

	/**
	 * Creates an instance of Gallery_Image. Deletes the original.
	 * Generates the thumbnail and resizes the full image.
	 * @param string $path The absolute path to the full-size image
	 * @param Gallery $instance The related Gallery instance
	 * @return instance
	 * @throws Exception If invalid configuration or images present
	 */
	public function __construct ($path, $instance) {
		$this->path = $path;
		$this->instance = $instance;

		if (dirname($this->path).'/' == $this->instance->base.$this->instance->options['full_directory']) {
			$this->thumbnailURI = $this->instance->options['base_uri'].$this->instance->options['thumbnail_directory'].basename($this->path);
			$this->fullURI = $this->instance->options['base_uri'].$this->instance->options['full_directory'].basename($this->path);
			list ($this->thumbnailWidth, $this->thumbnailHeight) = getimagesize($this->instance->base.$this->instance->options['thumbnail_directory'].basename($this->path));
		}
		else {
			$this->thumbnailURI = $this->instance->options['base_uri'].$this->instance->options['thumbnail_directory'].$this->resize('thumbnail');
			$this->fullURI = $this->instance->options['base_uri'].$this->instance->options['full_directory'].$this->resize('full');
			unlink($this->path);
		}

		$ext = pathinfo($this->path, PATHINFO_EXTENSION);
		$this->title = substr(basename($this->path), 0, -strlen($ext)-1);
	}

	/**
	 * Resizes a full-size image or thumbnail according to options.
	 * @param string $prefix 'full' or 'thumbnail'
	 * @return string The name of the generated file
	 * @throws Exception If invalid configuration or images present
	 */
	private function resize ($prefix) {
		if ($this->instance->options[$prefix.'_resize'] == Gallery::RESIZE_NONE) {
			copy($this->path, $this->instance->options['base_uri'].$this->instance->options['thumbnail_directory'].basename($this->path));
			list ($w, $h) = getimagesize($this->path);
			$this->thumbnailWidth = $w;
			$this->thumbnailHeight = $h;
			return basename($this->path);
		}

		$ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
		$f = substr(basename($this->path), 0, -strlen($ext)-1).'.jpg';

		$resized = null;
		$type = $ext != 'jpg' ? $ext : 'jpeg';
		$fn = 'imagecreatefrom'.$type;
		if ($image = $fn($this->path)) {
			if ($this->instance->options[$prefix.'_resize'] == Gallery::RESIZE_CROP) {
				$targetAspect = $this->instance->options[$prefix.'_width']/$this->instance->options[$prefix.'_height'];
				list ($origWidth, $origHeight) = getimagesize($this->path);
				$origAspect = $origWidth/$origHeight;

				$srcX = 0;
				$srcY = 0;
				$srcW = $origWidth;
				$srcH = $origHeight;
				
				if ($origAspect < $targetAspect) {
					$srcH = $origWidth/$targetAspect;
					$srcY = floor(($origHeight-$srcH)/2);
				}
				else if ($origAspect > $targetAspect) {
					$srcW = $targetAspect*$origHeight;
					$srcX = floor(($origWidth-$srcW)/2);
				}

				if ($prefix == 'thumbnail') {
					$this->thumbnailWidth = $this->instance->options[$prefix.'_width'];
					$this->thumbnailHeight = $this->instance->options[$prefix.'_height'];
				}

				$resized = imagecreatetruecolor($this->instance->options[$prefix.'_width'], $this->instance->options[$prefix.'_height']);
				imagecopyresized($resized, $image, 0, 0, $srcX, $srcY, $this->instance->options[$prefix.'_width'], $this->instance->options[$prefix.'_height'], $srcW, $srcH);
			}
			else if ($this->instance->options[$prefix.'_resize'] == Gallery::RESIZE_ASPECT) {
				$origWidth = imagesx($image);
				$origHeight = imagesy($image);
				$scale = min(1, min($this->instance->options[$prefix.'_width']/$origWidth, $this->instance->options[$prefix.'_height']/$origHeight));
				$targetWidth = floor($scale*$origWidth);
				$targetHeight = floor($scale*$origHeight);

				if ($prefix == 'thumbnail') {
					$this->thumbnailWidth = $targetWidth;
					$this->thumbnailHeight = $targetHeight;
				}

				$resized = imagecreatetruecolor($targetWidth, $targetHeight);
				imagecopyresized($resized, $image, 0, 0, 0, 0, $targetWidth, $targetHeight, $origWidth, $origHeight);
			}
			else if ($this->instance->options[$prefix.'_resize'] == Gallery::RESIZE_NONE) {
				if ($prefix == 'thumbnail') {
					list ($w, $h) = getimagesize($this->instance->base.$this->instance->options['thumbnail_directory'].basename($this->path));
					$this->thumbnailWidth = $w;
					$this->thumbnailHeight = $h;
				}
			}
			else {
				throw new Exception('Invalid value for $options['.$prefix.'_resize] of Gallery::__construct.');
			}

			if (file_exists($p = $this->instance->base.$this->instance->options[$prefix.'_directory'].$f)) {
				unlink($p);
			}
			imagejpeg($resized, $p, $this->instance->options[$prefix.'_quality']);
		}
		else {
			throw new Exception('Full-size image '.$this->path.' could not be converted because it was not a valid image.');
		}
		return $f;
	}

	/**
	 * A magic method that returns the value of the requested property of this instance.
	 * @var string $property The name of the property.
	 * @return mixed The value of the property
	 * @throws Exception If property does not exist
	 */
	public function __get ($property) {
		if (property_exists($this, $property)) {
			return $this->$property;
		}
		throw new Exception('Property '.$property.' of class Gallery does not exist.');
	}
}
?>